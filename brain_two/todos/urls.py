from django.urls import path
from todos.views import (
    todo_list_list,
    detail_todo,
    create_todo,
    edit_todo,
    delete_todo,
    create_todo_item,
    edit_todo_item,
)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", detail_todo, name="detail_todo"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", edit_todo, name="edit_todo"),
    path("<int:id>/delete/", delete_todo, name="delete_todo"),
    path("items/create/", create_todo_item, name="create_todo_item"),
    path("items/<int:id>/edit/", edit_todo_item, name="edit_todo_item"),
]
