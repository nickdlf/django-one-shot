from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos/list.html", context)


def detail_todo(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": todo_item,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # we want to setup author information first
            # false will prevent saving of the form to database
            # recipe = form.save(False)
            # tie author to LOGGED IN user
            list = form.save()
            return redirect("detail_todo", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "create_form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_instance)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("detail_todo", id=todo_instance.id)
    else:
        form = TodoListForm(instance=todo_instance)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("detail_todo", id=list.id)
    else:
        form = TodoItemForm()

    context = {
        "item_form": form,
    }
    return render(request, "todos/item_create.html", context)


def edit_todo_item(request, id):
    todo_item_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_instance)
        if form.is_valid():
            todo_item_instance = form.save()
            return redirect("detail_todo", id=todo_item_instance.id)
    else:
        form = TodoItemForm(instance=todo_item_instance)

    context = {
        "edit_item_form": form,
    }
    return render(request, "todos/edit_item.html", context)
